function [co, version] = circular3CColormap(nRange, isDemoPlot)
% 3 color-circular colormap scheme.
%
% generates a colormap for a circular variable (esp phase of oscillation)
% black to cyan through green and cyan to black through blue.
%
% Copyright 2017 Memming. BSD license.

version = 0.5; % developer's initial version

if nargin < 1
    nRange = 3*32;
end

if nargin < 2
    isDemoPlot = false;
end

%%
green = [0, 1, 0];
blue  = [0, 0, 1];
%cyan  = [0, 1, 1];
%black = [0, 0, 0];
%nRange = 6*60;
%halfRange = linspace(0, 1, nRange/2);
%co = [halfRange' * green; fliplr(halfRange)' * blue];

nRangeInternal = round(nRange/6)*6;
trdRange = linspace(0, 1, nRangeInternal/3);
%co = [trdRange' * green; fliplr(trdRange)' * blue];

sixRange = linspace(0, 1, nRangeInternal/6);

%bridgeRange = (tanh((trdRange-0.5)*5)+1)/2;
%bridgeRange = [zeros(size(sixRange)), tanh((sixRange)*3)];
%bridgeRange = [tanh((sixRange)*3),ones(size(sixRange))];
bridgeRange = [sixRange,ones(size(sixRange))];
%bridgeRange = trdRange;
%bridgeRange = atanh((trdRange-0.5)*0.99)+1;
%bridgeRange = bridgeRange - min(bridgeRange);
%bridgeRange = bridgeRange / max(bridgeRange);
coBridge = bridgeRange' * blue + fliplr(bridgeRange)' * green;
%coBridge = coBridge ./ (max(coBridge, [], 2) * [1,1,1]);
%coBridge(:, 2:3) = coBridge(:, 2:3) .* (1 + 0.5 * exp(-((trdRange'-0.5)*10).^2) * [1,1]);

co = [sqrt(trdRange)' * green; coBridge; fliplr(trdRange.^0.3)' * blue];

co = interp1(linspace(0,1,size(co, 1)), co, linspace(0,1,nRange), 'spline');
co(co > 1) = 1; co(co < 0) = 0; % fix interpolation errors

%%
if isDemoPlot
    %%
    figure; clf
    nX = 256;
    xr = linspace(-1, 1, nX);
    [XX, YY] = meshgrid(xr, xr);
    ZZ = XX + 1i * YY;
    %idx = round((angle(ZZ(:)) + pi)/pi/2 * nRange);
    idx = round(mod(angle(ZZ(:)), 2*pi)/pi/2 * nRange);
    
    idx(idx == 0) = 1;
    idx(isnan(idx)) = nRange;
    c = co(idx, :);
    c = reshape(c, [nX,nX,3]);
    scale = abs(ZZ(:));
    outsidx = scale > 1;
    scale(outsidx) = nan;
    c = c .* repmat(reshape(scale,nX,nX),1,1,3);
    c(isnan(c)) = 1;
    image(xr, xr, c);
    axis xy
    axis equal
    xlim([-1.1, 1.1])
    ylim([-1.1, 1.1])
    set(gca, 'XTick', [], 'YTick', [], 'box', 'off');

    %%
    figure; clf;
    
    subplot(3,1,1)
    plot(linspace(0, 2*pi, size(co,1)), co, 'LineWidth', 2);
    grid on;
    line(pi * [1,1], [0,1]);
    xlim([0, 2*pi]);
    lh = legend('R', 'G', 'B');
    set(lh, 'box', 'off')
    
    subplot(3,1,2)
    imagesc(linspace(0, 2*pi));
    colormap(co);
    %imshow(reshape(kron(co,ones(50,1)), 50, [], 3), 'InitialMagnification', 500)
    
    subplot(3,1,3)
    testLinear = linspace(0, pi); testLinear = [testLinear, fliplr(testLinear)];
    colormap(co);
    imagesc(testLinear);
    %colorbar
end