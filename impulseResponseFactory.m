function [ir, tr, param] = impulseResponseFactory(name, binSize, param)
% Provides a library of 'biologically plausible' impulse responses
% [ir, tr, param] = impulseResponseFactory(name, binSize, param)
% Desined to aid quick access to reasonable simulated models.
% Impuse responses have approximately constant L1 norm unless noted
% otherwise.
%
% Input
%    name: string identifying the impulse response function. (param)
%          'exp': exponential decay (tau)
%          'alpha': alpha function (tau)
%          'gamma': gamma distribution function (tau, k)
%    binSize: (1) time bin size (default: 1e-3)
%    param: parameter for the impulse response function
%
% Output
%    ir: [nT x 1] impulse response function
%    tr: [nT x 1] uniformly sampled time bin range
%    param: possibly updated parameters (e.g. default values)

if nargin < 2
    binSize = 1e-3;
end

if nargin < 3
    param = [];
end

switch lower(name)
    case {'exp-decay', 'exp'},
        % Plain old exponential decay
        % special case of gamma distribution function with k=1
        if isempty(param)
            param = 20e-3;
        end
        
        tau = param;
        tr = 0:binSize:ceil(10*tau/binSize)*binSize;
        ir = exp(-tr/tau)/tau;
    case 'alpha',
        % Alpha function, often used as a synaptic transfer function
        % shape: t * exp(-t)
        % special case of gamma distribution function with k=2
        if isempty(param)
            param = 20e-3;
        end
        
        tau = param;
        tr = 0:binSize:ceil(10*tau/binSize)*binSize;
        ir = tr/tau .* exp(-tr/tau);
        
    case 'gamma',
        % Gamma distribution function (a.k.a. Erlang distribution)
        % or Gamma basis functions
        if isempty(param)
            param.k = 1.5;
            param.tau = 20e-3;
        end
        
        tau = param.tau;
        k = param.k;
        assert(k > 0)
        assert(tau > 0)
        t999 = gaminv(0.999,k,tau);
        tr = 0:binSize:ceil(t999/binSize)*binSize;
        ir = gampdf(tr,k,tau);
    otherwise
        error('unknown impulse response name [%s]', name);
end

if nargout < 1
    figure(8937);
    plot(tr, ir);
    xlabel('time'); title(name);
end

ir = ir(:);
tr = tr(:);