function savefigpp(fig, fsize, fname, args)
% example: savefigpp(fig, [4,8], '%s/nonlinearTransform_%03d', {outDir, kNeuron});
drawnow;
set(fig, 'PaperUnits', 'inches');
set(fig, 'PaperPosition', [0,0,fsize(1),fsize(2)], 'PaperSize',fsize);
saveas(fig, sprintf([fname, '.png'],args{:}));
saveas(fig, sprintf([fname, '.pdf'],args{:}));