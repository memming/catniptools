function isMore = iterate(ea, method, verbose)
% Iterate through the subplots specified as the method
% Note that this method does NOT update the easy axes object.

persistent iterColumn iterRow

if nargin < 3
    verbose = false;
end

if isempty(iterColumn)
    iterColumn = 0; iterRow = 0;
end

if strcmp(method, 'reset') 
    iterColumn = 0; iterRow = 0;
    isMore = false;
    return;
end

if strcmp(method, 'default') ...
	|| strcmp(method, 'upperright') ...
	|| strcmp(method, 'all')

    flag = true;
    while flag
	if (iterColumn == ea.nColumn) && (iterRow == ea.nRow)
	    iterColumn = 0; iterRow = 0;
	    isMore = false;
	    return;
	end
	if iterColumn == 0 || iterRow == 0 % if it is a freash start
	    iterColumn = 1;
	    iterRow = 1;
	else
	    iterColumn = iterColumn + 1;
	    if iterColumn > ea.nColumn
		iterColumn = 1;
		iterRow = iterRow + 1;
	    end
	end
	if strcmp(method, 'default') && ea.axes(iterColumn, iterRow) == 0
	    flag = true;
	elseif strcmp(method, 'upperright') ...
		&& (ea.axes(iterColumn, iterRow) == 0 ...
		|| iterColumn == 1 || iterRow == ea.nRow)
	    fprintf('>>> %d %d\n', iterColumn, iterRow);
	    flag = true;
	elseif strcmp(method, 'all')
	    flag = false;
	else
	    flag = false;
	end
    end
else
    error('Unknown method [%s]', method);
end
if verbose
    fprintf('(%2d, %2d)\n', iterColumn, iterRow);
end
ea = subplot(ea, iterColumn, iterRow);
isMore = true;
