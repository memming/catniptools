function [ea] = subplot(ea, column, row)

% check condition
if column < 0 || column > ea.nColumn
    error('violated [0 < column <= %d]', ea.nColumn);
end
if row < 0 || row > ea.nRow
    error('violated [0 < row <= %d]', ea.nRow);
end

if ea.axes(column, row) == 0
    ax = axes('Position', [...
	ea.hspace + (column - 1) * ea.width, ...
	ea.vspace + (ea.nRow - row) * ea.height, ea.width, ea.height]);
    ea.axes(column, row) = ax;
    if nargout == 0; error('you need to update your Easy Axes object!'); end
else
    ax = ea.axes(column, row);
    set(gcf, 'CurrentAxes', ax);
end
