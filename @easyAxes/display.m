function display(ea)
% Displays basic information about the object

fprintf('Memming''s Easy Axes object (%d,%d)\n', ea.nColumn, ea.nRow);
fprintf('spacing (%f, %f), axis size (%f, %f)\n', ...
    ea.hspace, ea.vspace, ea.height, ea.width);
