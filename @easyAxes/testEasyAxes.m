ea = easyAxes(3,4);
ea = subplot(ea,1,2);
line([0,1], [0,2]);
line([1,0], [0,2]);

for c = 1:3
    for r = 1:4
	ea = subplot(ea,c,r);
    end
end

while iterate(ea, 'all')
    line([0,1], [0,2]);
    set(gca, 'Color', [0.3 0.3 0.4])
    set(gca, 'Box', 'On');
end

while iterate(ea, 'upperright')
    set(gca, 'Color', [1 0 1])
    line([0,1], [2,0]);
    set(gca, 'YTickLabel', []);
    set(gca, 'XTickLabel', []);
end
