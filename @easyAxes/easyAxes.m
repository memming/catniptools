function ea = easyAxes(varargin)
% Constructor for Easy Axes, useful for subplotting without space
%
% Input:
%   nColumn: number of columns > 0
%   nRow: number of rows > 0
%   hspace: horizontal space from border (0 <= hspace < 1)
%   vspace: vertical space from border (0 <= vspace < 1)
%
% +-------------------------------------------+
% | +---------+---------+---------+---------+ |
% | |  (1,1)  |  (2,1)  |  (3,1)  |  (4,1)  | |
% | +---------+---------+---------+---------+ |
% | |  (1,2)  |  (2,2)  |  (3,2)  |  (4,2)  | |
% | +---------+---------+---------+---------+ |
% | |  (1,3)  |  (2,3)  |  (3,3)  |  (4,3)  | |
% | +---------+---------+---------+---------+ |
% +-------------------------------------------+

% constructor with no argument
if nargin ~= 0 && isa(varargin(1), 'easyAxes'); ea = arg; return; end

% Trick from http://www.ee.columbia.edu/~marios/matlab/matlab_tricks.html
defaultValues = {1, 1, 0.05, 0.05};
nonemptyIdx = ~cellfun('isempty', varargin);
defaultValues(nonemptyIdx) = varargin(nonemptyIdx);
[ea.nColumn ea.nRow ea.hspace ea.vspace] = deal(defaultValues{:});
ea.width = (1 - 2 * ea.hspace) / ea.nColumn;
ea.height = (1 - 2 * ea.vspace) / ea.nRow;
ea.axes = zeros(ea.nColumn, ea.nRow);
ea = class(ea, 'easyAxes');
iterate(ea, 'reset');

end
