function [g, gfilt] = gsmooth(y, sig, skipNormalization)
% gsmooth - smooth vector or matrix by filtering with a Gaussian 
%
% g = gsmooth(x, sig);
%
% Inputs:
%    y: [MxN] - matrix or vector (if matrix, operates along columns only)  
%    sig: [1x1] - stdev of smoothing Gaussian
%    skipNormalization: (optional/1) if true, skips normalization.
%                       by default normalizes filter to have unit-norm.
%
% Output:
%    g: [MxN] - smoothed signal
%    gfilt: convolved gaussian
%
% See also: bcsmooth

if nargin < 3
    skipNormalization = false;
end

if (sig <= 0)  % Return original if no smoothing width 
    g = y;
else
    [len,wid] = size(y);
    if (len == 1)  % Flip to column vector
        y = y';
        flipped = true;
        len = wid;
    else
        flipped = false;
    end
    [~,nx] = size(y);
    
    nflt = max(min(len, sig*5),3);
    x = (-nflt:nflt)';
    gfilt = normpdf(x, 0, sig);
    gfilt = gfilt./norm(gfilt);
    
    g = zeros(size(y));
    
    if skipNormalization
        for j = 1:nx
            g(:,j) = conv(y(:,j), gfilt, 'same');
        end
    else 
        o = conv(ones(size(y,1),1), gfilt, 'same');
        for j = 1:nx
            g(:,j) = conv(y(:,j), gfilt, 'same') ./ o;
        end
    end
    
    if flipped
        g = g';
    end
end
