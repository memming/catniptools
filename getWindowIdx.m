function [dataIdx, targetIdx] = getWindowIdx(t, windowSizeL, windowSizeR, trialLength)
% [dataIdx, targetIdx] = getWindowIdx(t, windowSizeL, windowSizeR, trialLength);
% Utility function for obtaining the index for a clipping window.
% Two index references for a window centered around t.
%
%        windowSizeL      windowSizeR
%      |------------- t ---------------|
%      1         targetIdx             windowSizeL + windowSizeR + 1
%          >------------------<
%          |     dataIdx      |
%          1                  trialLength
%
% ex) 
% x = [1 2 3 4 5];
%%     [ - - t - - ]
%%      [2 3 4 5] <-- dataIdx
%%      [1 2 3 4] <-- targetIdx
% [dataIdx, targetIdx] = getWindowIdx(4, 2, 2, length(x));
% [dataIdx, targetIdx] = getWindowIdx(1, 2, 2, length(x));
% x =       [1 2 3 4 5];
%%     [ - - t - - ]
%%          [1 2 3] <-- dataIdx
%%          [3 4 5] <-- targetIdx

tBegin1 = max(t - windowSizeL, 1);
tEnd1 = min(t + windowSizeR, trialLength);
targetIdx = (windowSizeL-t+tBegin1+1):(windowSizeL-t+tEnd1+1);
dataIdx = tBegin1:tEnd1;
