function [f,g,H] = sumLossHandles(fh1, fh2, varargin)
% Usage: f = @(x) sumLossHandles(@(x) loss(x), @(x) prior(x), x);
%
% It is often useful to add multiple loss functions for optimization (e.g.,
% adding negative log-likelihood and a regularizer or log-prior)
% However, due to limitation in MATLAB's syntax, we can't combine 
% function handles with multiple outputs easily.
% This utility function should make that easier!

switch nargout
    case {0, 1}
        f1 = fh1(varargin{:});
        f2 = fh2(varargin{:});
        f = f1 + f2;
    case 2
        [f1, g1] = fh1(varargin{:});
        [f2, g2] = fh2(varargin{:});
        f = f1 + f2;
        g = g1 + g2;
    case 3
        [f1, g1, H1] = fh1(varargin{:});
        [f2, g2, H2] = fh2(varargin{:});
        f = f1 + f2;
        g = g1 + g2;
        H = H1 + H2;
    otherwise
        error('%s only supports up to 3 outputs', mfilename);
end
