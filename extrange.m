function [newmin, newmax] = extrange(xx, proportion)
% find a slightly extended range of the data. Useful for plotting range.
% Input
%   xx: (N x 1) 1-dimensional data (real)
%   proportion: (1/optional) 1 is just the range, 1.5 gives 50% extended
%               range default is 10%

if nargin < 2
    proportion = 1.1;
end

rangeX = (max(xx) - min(xx))/2;
midPoint = (max(xx) + min(xx))/2;

newmin = midPoint - (rangeX * proportion);
newmax = midPoint + (rangeX * proportion);
