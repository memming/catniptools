function [f,g,H] = l2reg(w, lambda)
% Good old l2-norm regularizer (aka Ridge / Tikhonov regularization)
% the ridge parameter (lambda) should be non-negative

f = lambda * sum(w.^2) / 2;
g = lambda * w;
H = lambda * diag(ones(numel(w),1));
